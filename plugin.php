<?php

namespace NML {

/*
Plugin Name: Educational Mail List
Description: It creates a mail list for you.
Version: 0.0.1
Author: Nadir Özkan
Requires at least: 5.2
Requires PHP: 7.2
Author URI: http://nadirozkan.xyz
*/

require "customPostTypes.php";
require "customAdmin.php";
require "mailFormShortcode.php";

// namespace PDEV;
// class RandomPosts {
//     public static function randomize( $query ) {
//         if ( $query->is_main_query() && $query->is_home() ) {
//             $query->set( 'orderby', 'rand' ); // ohaaaaa random sırala diye bir seçenek varmış ya laaa!
//         }
//     }
// }
// add_action( 'pre_get_posts', [ 'PDEV\RandomPosts', 'randomize' ] ); // Bir namespace içindeki bir classın içindeki fonksiyona referans ver.

add_action( "wp_enqueue_scripts", "NML\loadPublicScripts");

function loadPublicScripts() {
    // load this script in footer...
    wp_register_script( 'abone-kaydet-js', plugins_url( 'js/public/aboneKaydet.js',__FILE__ ), array('jquery'), "0.0.1", true );
    wp_enqueue_script('abone-kaydet-js');

    wp_register_style( 'mail-form-style',  plugins_url( 'css/public/mailForm.css',__FILE__ ) );
    wp_enqueue_style( "mail-form-style");

    // neden register ve enqueue fonksiyonları ayrı aryı araştır.

    $php_vars = array(
        'admin_url' => admin_url(),
        'ajax_url' => admin_url('admin-ajax.php')
    );

    wp_localize_script( 'abone-kaydet-js', 'kaydetMeta', $php_vars );
}

function listAllArgs(){
    foreach (func_get_args() as $argName => $argValue) {
        echo "<br /> $argName => $argValue";
    }
    die();
}

} // namespace

<?php

namespace NML {

    // abone kaydetmek ile ilgili tüm kodlar burada...
    require "aboneKaydet.php";

    // register all our custom shortcoedes
    add_action( 'init', 'NML\register_shortcodes');

    // hint: register ajax actions
    add_action('wp_ajax_nopriv_aboneKaydet', 'NML\aboneKaydet'); // regular website visitor
    add_action('wp_ajax_aboneKaydet', 'NML\aboneKaydet'); // admin user

    function register_shortcodes() {
        add_shortcode( 'mail_form', 'NML\mail_form_shortcode' ); // Dosya isminin başına namespace eklemeyi unutma!
    };

    // $content bu shortcode'u etrafına sardığımız kısım, bir bakıma react'taki children'a denk geliyor.
    // Bu shortcode tagi herhangi birşeyin etrafına sarılmazsa $content varsayılan olarak boş gelsin.
    function mail_form_shortcode($args, $content="") {

        $firstName = "";
        $lastName = "";
        $email = "";
        $errorDiv = "";

        // get the list id
        $list_id = 0;
        if( isset($args['id']) ) $list_id = (int)$args['id'];

        $error = isset($_GET["nml_error"]) ? $_GET["nml_error"] : "";
        $success = "";

        if (isset($_GET["save_status"])) {
            $firstName = ' value="'. $_GET["nml_first_name"] . '"';
            $lastName = ' value="'. $_GET["nml_last_name"] . '"';
            $email =  ' value="'. $_GET["nml_email"] . '"';
            if ((int)$_GET["save_status"] == 1) {
                $success = "Aboneliğiniz başarıyla kaydedildi.";
            }
        }

        global $wp;
        $current_url = home_url( add_query_arg( array(), $wp->request ) );
        $current_slug = add_query_arg( array(), $wp->request );

        $errorDiv = "<div class='nml_post_error'>$error</div>";
        $successDiv = "<div class='nml_post_success'>$success</div>";

        $nonceField = wp_nonce_field( 'mail_form_submit', 'nml_mail_form_nonce', false, false );

        $actionPage = admin_url("/admin-ajax.php?action=aboneKaydet");
        $output = '

            <div class="nml">

                <form id="nml_form" name="nml_form" class="nml-form" method="post"
                    action="'. $actionPage . '"
                >
                    '. $nonceField .'
                    <input type="hidden" name="nml_list_id" value="'. $list_id .'">
                    <input type="hidden" name="nml_current_slug" value="'. $current_slug .'">

                    <p class="nml-input-container">
                        <label>Your Name</label><br />
                        <input type="text" name="nml_first_name" placeholder="First Name"'. $firstName .'/>
                        <input type="text" name="nml_last_name" placeholder="Last Name"'. $lastName .' />
                    </p>

                    <p class="nml-input-container">
                        <label>Your Email</label><br />
                        <input type="email" name="nml_email" placeholder="ex. you@email.com"'. $email.'/>
                    </p>';

                    $output.=$errorDiv;
                    $output.=$successDiv;

                    // including content in our form html if content is passed into the function
                    if( strlen($content) ) {
                        // wpautop => content etrafına p tagi ekle...
                        $output = $output . '<div class="nml-content">'. wpautop($content) .'</div>';
                    }

                    // completing our form html
                    $output .= '<p class="nml-input-container">
                        <input style="margin: 0.5em 0; margin-left:2px" type="submit" name="nml_submit" value="Sign Me Up!" />
                    </p>

                </form>

            </div>';

            return $output;

    };

} // namespace

?>

<?php

namespace NML {

    function getAbonePostId($email) {

        global $wpdb;
        // print_r($wpdb);
        $sql_str = "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key= 'nml_email' AND meta_value='$email'";
        $result = $wpdb->get_results($sql_str);

        if ( !is_array( $result ) || empty( $result ) ) return 0;

        return $result[0]->post_id;

    }

    function aboneBuListeyeKayitli($post_id, $list_id) {

        global $wpdb;
        // print_r($wpdb);
        $sql_str = "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key= 'nml_mail_list'";
        $sql_str.= " AND meta_value=$list_id and post_id=$post_id";
        $result = $wpdb->get_results($sql_str);

        if ( !is_array( $result ) || empty( $result ) ) return false;

        return true;

    }

    function aboneGuncelle($data, $post_id) {
        $list_id = $data['nml_list_id'];
        if (!aboneBuListeyeKayitli($post_id, $list_id)) {
            add_post_meta( $post_id, "nml_mail_list", $data['nml_list_id']);
        };
        // Her durumda ad ve soyad post meta datasını güncelle.
        update_post_meta( $post_id, "nml_first_name", $data['nml_first_name']);
        update_post_meta( $post_id, "nml_last_name", $data['nml_last_name']);
    }

    function aboneEkle($data) {
        $post_id = wp_insert_post( array(
            'post_type' => "mail_subscriber",
            'post_title' => $data['nml_first_name'] . " " . $data['nml_last_name'],
            'post_status' => "publish"
        ), true);

        add_post_meta( $post_id, "nml_first_name", $data['nml_first_name']);
        add_post_meta( $post_id, "nml_last_name", $data['nml_last_name']);
        add_post_meta( $post_id, "nml_email", $data['nml_email']);
        add_post_meta( $post_id, "nml_mail_list", $data['nml_list_id']);

    }

    function validateForm($data) {

        if ($data["nml_first_name"]=="") {
            $data["nml_error"] = "Ad alanı boş bırakılamaz.";
            return $data;
        }

        if ($data["nml_last_name"]=="") {
            $data["nml_error"] = "Soyad alanı boş bırakılamaz.";
            return $data;
        }

        if ($data["nml_email"]=="") {
            $data["nml_error"] = "Email alanı boş bırakılamaz.";
            return $data;
        }

        if (!is_email($data["nml_email"])) {
            $data["nml_error"] = "Lütfen geçerli bir email adresi giriniz.";
            return $data;
        }

        return $data;
    }

    function aboneKaydet(){

        $results = array(
            'save_status' => 0,
            'nml_first_name' => isset($_POST["nml_first_name"]) ? esc_attr(sanitize_text_field( $_POST["nml_first_name"] )) : "",
            'nml_last_name' => isset($_POST["nml_last_name"]) ? esc_attr(sanitize_text_field( $_POST["nml_last_name"] )) : "",
            'nml_email' => isset($_POST["nml_email"]) ? esc_attr(sanitize_text_field( $_POST["nml_email"] )) : "",
            'nml_list_id' => (int)$_POST['nml_list_id'],
            'nml_current_slug' => $_POST["nml_current_slug"]
        );

        $posted_from_ajax = $_POST["ajax"];

        try {

            // Verify nonce
            if( !isset( $_POST['nml_mail_form_nonce'] ) || !wp_verify_nonce( $_POST['nml_mail_form_nonce'], 'mail_form_submit' ) ) {
                die("Nonce could'nt be verified. Sorry!");
            }

            $results = validateForm($results);
            if (isset($results["nml_error"])) {
                $results["save_status"] = 0;
                redirectWithArgs($results, esc_url(site_url($results["nml_current_slug"])), $posted_from_ajax);
            }

            $abonePostId = getAbonePostId($results["nml_email"]);

            if ($abonePostId != 0) { // Bu email adresi kayıtlı
                aboneGuncelle($results, $abonePostId);
            } else {
                aboneEkle($results);
            }

            $results["save_status"] = 1; // kayıt işlemi başarılı

        } catch (Exception $e) {
            throw $e;
        }

        redirectWithArgs($results, esc_url(site_url($results["nml_current_slug"])), $posted_from_ajax);
    }

    function returnJson( $php_array ) {
        $json_result = json_encode( $php_array );
        die( $json_result );
        exit;
    }

    function toQueryString($arr){
        if (count($arr)==0) return "";
        $result ="?";
        foreach ($arr as $key => $value) {
            $result .= "$key=$value&";
        }
        return $result;
    }

    function redirectWithArgs( $php_array, $location, $posted_from_ajax ) {
        if ($posted_from_ajax) {
            returnJson($php_array);
            exit();
        } else {
            header("Location: $location" . toQueryString($php_array));
            exit();
        }

    }

} // namespace

?>

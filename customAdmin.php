<?php

namespace NML {

    add_action('admin_head-edit.php', 'NML\registerCustomAdminTitles');
    add_action('add_meta_boxes_mail_subscriber','NML\register_mail_subscriber_metabox');
    add_action('save_post','NML\saveMailSubscriber', 10, 2);
    add_filter('manage_edit-mail_subscriber_columns','NML\displayColumnHeaders');
    add_filter('manage_edit-mail_list_columns','NML\displayMailListColumnHeaders');
    add_filter('manage_mail_subscriber_posts_custom_column','NML\getMailSubscriberColumnData',1,2);

    add_action('admin_menu', 'NML\addMenuItems');

    // Add custom metabox => the box to show in the admin panel for mail_subscriber post type

    function register_mail_subscriber_metabox($post){
        // Burada argüman olan boş bir mail_subscriber kaydı geliyor.
        // print_r($post);
        add_meta_box(
            'nml-mail-subscriber-details',
            'Subscriber Details',
            'NML\showMailSubscriberMetabox',
            'mail_subscriber',
            'normal',
            'default'
        );
    };

    function getMailListTypes($selectedKeys) {
        global $wpdb;

        // $wpdp->posts => "wp_posts"
        // print_r($selectedKeys);

        $list_query = $wpdb->get_results("SELECT * FROM {$wpdb->posts} WHERE post_type = 'mail_list' AND post_status IN ('publish','draft')");

        if ( !is_null($list_query) ) {

            foreach($list_query as $list ){
                $checked = ( in_array($list->ID, $selectedKeys) ) ? 'checked="checked"' : '';
                echo '<li><label><input type="checkbox" name="nml_mail_list[]" value="'. $list->ID .'" '. $checked .' /> '. $list->post_title .'</label></li>';
            }

        }
    }

    function showMailSubscriberMetabox(){
        // İlgili dosyada bir nonce yarat
        wp_nonce_field(basename(__FILE__), 'mail_subscriber_nonce');

        global $post;
        $post_id = $post->ID;

        // Get the values from database
        $first_name = !empty(get_post_meta( $post_id, "nml_first_name", true)) ? get_post_meta( $post_id, "nml_first_name", true) : "";
        $last_name = !empty(get_post_meta( $post_id, "nml_last_name", true)) ? get_post_meta( $post_id, "nml_last_name", true) : "";
        $email = !empty(get_post_meta( $post_id, "nml_email", true)) ? get_post_meta( $post_id, "nml_email", true) : "";

        $lists = !empty(get_post_meta( $post_id, "nml_mail_list", false)) ? get_post_meta( $post_id, "nml_mail_list", false) : [];

        ?>
            <style>
                .nml-field-row {
                    display: flex;
                    flex-flow: row nowrap;
                    flex: 1 1;
                    margin-bottom: 0.5em;
                }
                .nml-field-container {
                    position: relative;
                    flex: 1 1;
                    margin-right: 1em;
                }
                .nml-field-container label {
                    font-weight: bold;
                }
                .nml-field-container label span {
                    color: red;
                }
                .nml-field-container label ul {
                    list-style: none;
                    margin-top:0;
                }
                .nml-field-container label ul label {
                    font-weight: normal;
                }

            </style>

            <div class="nml-field-row">
                <div class="nml-field-container">
                    <label>Ad <span>*</span></label><br>
                    <input type="text" name="nml_first_name" value="<?php echo $first_name; ?>" required="required" class="widefat">
                </div>
                <div class="nml-field-container">
                    <label>Soyad <span>*</span></label><br>
                    <input type="text" name="nml_last_name" value="<?php echo $last_name; ?>"  required="required" class="widefat">
                </div>
            </div>
            <div class="nml-field-row">
                <div class="nml-field-container">
                    <label>Email <span>*</span></label><br>
                    <input type="text" name="nml_email" value="<?php echo $email; ?>"  required="required" class="widefat">
                </div>
            </div>
            <div class="nml-field-row">
                <div class="nml-field-container">
                    <label>Listeler</label><br>
                    <ul>
                        <?php getMailListTypes($lists); ?>
                    </ul>
                </div>
            </div>

        <?php
    }

    function pluginOptions() {
        echo "<h3>Here is plugin options page</h3>";
    }

    function importSubscribers() {
        echo "<h3>Here is import subscribers page</h3>";
    }

    function mailListDashboard() {
        echo "<h2 style='color:green'>Here is the admin page</h2>";
    }

    function addMenuItems() {

        $top_menu_item = 'mailListDashboard';
        add_menu_page( '', 'List Builder', 'manage_options', $top_menu_item, 'NML\mailListDashboard', 'dashicons-email-alt');

        // alt menü öğeleri
	    add_submenu_page( $top_menu_item, '', 'Dashboard', 'manage_options', $top_menu_item, 'NML\mailListDashboard' );
	    add_submenu_page( $top_menu_item, '', 'Email Lists', 'manage_options', 'edit.php?post_type=mail_list' );
	    add_submenu_page( $top_menu_item, '', 'Subscribers', 'manage_options', 'edit.php?post_type=mail_subscriber' );
	    add_submenu_page( $top_menu_item, '', 'Import Subscribers', 'manage_options', 'importSubscribers', 'NML\importSubscribers' );
	    add_submenu_page( $top_menu_item, '', 'Plugin Options', 'manage_options', 'pluginOptions', 'NML\pluginOptions' );

    }

    function saveMailSubscriber($post_id, $post) {
        // Verify nonce
        if( !isset( $_POST['mail_subscriber_nonce'] ) || !wp_verify_nonce( $_POST['mail_subscriber_nonce'], basename(__FILE__) ) ) {
            return $post_id;
        }

        // get the post type object
        $post_type = get_post_type_object($post->post_type);

        // check if the current user has permission to edit the post
        if( !current_user_can($post_type->cap->edit_post, $post_id) ) {
            return $post_id;
        }
            // get the posted data and sanitize it
        $first_name = ( isset($_POST['nml_first_name']) ) ? sanitize_text_field($_POST['nml_first_name']) : '';
        $last_name = ( isset($_POST['nml_last_name']) ) ? sanitize_text_field($_POST['nml_last_name']) : '';
        $email = ( isset($_POST['nml_email']) ) ? sanitize_text_field($_POST['nml_email']) : '';
        $lists = ( isset($_POST['nml_mail_list']) && is_array($_POST['nml_mail_list']) ) ? (array) $_POST['nml_mail_list'] : [];

        // finally write to Database
        update_post_meta( $post_id, "nml_first_name", $first_name);
        update_post_meta( $post_id, "nml_last_name", $last_name);
        update_post_meta( $post_id, "nml_email", $email);

        // Daha önceki list metadatasını sil
        delete_post_meta( $post_id, "nml_mail_list");

        if (!empty($lists)){
            foreach ($lists as $list_id) {
                add_post_meta( $post_id, "nml_mail_list", $list_id, false); // false => does'nt has to be uniqu
            }
        }

    }
    // Mail subscriber listesinin sütun başlıllarını buradan döndür.
    function displayColumnHeaders( $columns ) {

        // creating custom column header data
        $columns = array(
            'cb'=>'<input type="checkbox" />', // cb => checkbox
            'title'=>__('Subscriber Name'),
            'email'=>__('Email Address'),
        );

        // returning new columns
        return $columns;

    }

    function displayMailListColumnHeaders( $columns ) {

        // creating custom column header data
        $columns = array(
            'cb'=>'<input type="checkbox" />', // cb => checkbox
            'title'=>__('Liste Adı'),
        );

        // returning new columns
        return $columns;

    }

    function get_field($meta_key, $post_id) {
        global $wpdb;
        // print_r($wpdb);
        $sql_str = "SELECT meta_value FROM {$wpdb->postmeta} WHERE post_id = $post_id AND meta_key= '$meta_key'";
        $result = $wpdb->get_results($sql_str );
        // print_r($result);

        if ( !is_array( $result ) || empty( $result ) ) return "";

        return $result[0]->meta_value;

    }

    function registerCustomAdminTitles() {
        echo "registerCustomAdminTitles";
        add_filter(
            'the_title',
            'NML\getCustomAdminTitles',
            99,
            2
        );
    }

    function getCustomAdminTitles( $title, $post_id ) {
        // echo "getCustomAdminTitles";
        global $post;

        $output = $title;

        if( isset($post->post_type) ):
                    switch( $post->post_type ) {
                            case 'mail_subscriber':
                                    $fname = get_field('nml_first_name', $post_id );
                                    $lname = get_field('nml_last_name', $post_id );
                                    $output = $fname .' '. $lname;
                                    break;
                    }
            endif;

        return $output;
    }

    // Mail Subscriber listesinde görüntülenecek verileri buradan döndür.
    function getMailSubscriberColumnData($column_name, $post_id) {

        // setup our return text
        $output = '';
        switch( $column_name ) {

            case 'title':
                // get the custom name data
                $fname = get_field('nml_first_name', $post_id );
                $lname = get_field('nml_last_name', $post_id );
                $output .= $fname .' '. $lname;
                break;
            case 'email':
                // get the custom email data
                $email = get_field('nml_email', $post_id );
                $output .= $email;
                break;

        }

        // echo the output
        echo $output;
    }

} // namespace

?>

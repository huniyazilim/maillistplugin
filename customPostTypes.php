<?php

namespace NML {
    add_action( 'init', 'NML\register_custom_post_types' );

    function register_custom_post_types() {
        register_post_type( 'mail_subscriber', array(
            'labels' => array(
                'name' => "Mail Subscriber"
            ),
            'supports' => array('none'), // hiç bir şeyi göstermesin istiyorsan "none" olmalı...
            'public' => true,
            'has_archice' => false,
            'show_in_menu' => false,
            'menu_icon' => 'dashicons-email-alt',
            'exclude_from_search' => true,
        ) );
        register_post_type( 'mail_list', array(
            'labels' => array(
                'name' => "Mail List"
            ),
            'supports' => array('title'),
            'public' => true,
            'has_archice' => false,
            'show_in_menu' => false,
            'menu_icon' => 'dashicons-list-view',
            'exclude_from_search' => true,
        ) );
    };

} // namespace

?>

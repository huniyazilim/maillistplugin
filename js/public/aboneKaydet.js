jQuery(document).ready(function($){
    console.log("aboneKaydet.js");

    const wpAjaxUrl = kaydetMeta.ajax_url;

    const $form = $("form.nml-form");

    $form.bind("submit", function () {
        let formData = $form.serialize();
        formData += "&ajax=true";
        $.ajax({
            method : "post",
            url : wpAjaxUrl + "/?action=aboneKaydet",
            data : formData,
            cache : false,
            success : function (response, textStatus) {
                try {
                    const data = JSON.parse(response);
                    console.log(data);
                    if (data.save_status==1) {
                        alert("Abone başarıyla kaydedildi.");
                        $form[0].reset();
                    } else {
                        if (data.nml_error){
                            alert(data.nml_error);
                        }
                    }
                } catch (e) {
                    throw e;
                }
            },
            error : function(xhr, status, err){
                console.log(err);
            }
        })
        return false; // stop form submitting natively
    });

});
